from django.db import models
from model_utils import Choices
from model_utils.fields import StatusField
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.


class Business(models.Model):
    name = models.CharField(max_length=150)
    taxid = models.CharField(max_length=150)
    phone = PhoneNumberField(null=False, blank=False, unique=True,
                             help_text='Contact phone number')
    naics = models.IntegerField()
    is_profitable = models.BooleanField(default=False)
    has_bankrupted_in_last_7years = models.BooleanField(default=False)
    inception_date = models.DateField()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.name


class Owner(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    name = models.CharField(max_length=350, editable=False)
    email = models.CharField(max_length=200)
    phone = PhoneNumberField(null=False, blank=False, unique=True,
                             help_text='Contact phone number')
    date_of_birth = models.DateField()
    ssn = models.CharField(max_length=20)
    ownership_percentage = models.IntegerField()
    business_id = models.ForeignKey('Business', related_name='owners',
                                    on_delete=models.CASCADE)

    def save(self):
        self.name = first_name + " " + last_name
        super(Owner, self).save()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class Loan(models.Model):
    STATUS = Choices('Requested', 'In process', 'Pending', 'Approved', 'Denied')
    requested_amount = models.IntegerField()
    entity_type = models.CharField(max_length=15)
    stated_credit_history = models.IntegerField()
    status = StatusField(choices_name='STATUS', default="Requested")
    business_id = models.ForeignKey('Business', on_delete=models.CASCADE)

    def __str__(self):
        return self.business_id.name


class Address(models.Model):
    address1 = models.CharField(max_length=200)
    address2 = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=150)
    state = models.CharField(max_length=150)
    zip = models.IntegerField()
    business_id = models.ForeignKey('Business', on_delete=models.CASCADE,
                                    blank=True, null=True)
    owner_id = models.ForeignKey('Owner', on_delete=models.CASCADE,
                                 blank=True, null=True)

    def __str__(self):
        return self.business_id.name if self.business_id else self.owner_id.name
