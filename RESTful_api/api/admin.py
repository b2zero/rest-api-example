from django.contrib import admin
from .models import Business, Owner, Loan, Address
# Register your models here.

#
admin.site.register(Business)
admin.site.register(Owner)
admin.site.register(Loan)
admin.site.register(Address)
