from rest_framework import serializers

from .models import Loan


class LoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = ('id', 'business_id', 'status', 'requested_amount',
                  'entity_type', 'stated_credit_history')
